package escuela.java;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hgranjal on 12/06/2018.
 */
public class AlumnosFactory {

    public static List<NotaAlumno> getAlumnos() {
        List<NotaAlumno> notasAlumnos = new ArrayList<NotaAlumno>();
        NotaAlumno notaBilal = new NotaAlumno("Bilal", 13);
        NotaAlumno notaDani = new NotaAlumno("Dani", 14);
        NotaAlumno notaHelena = new NotaAlumno("Helena", 15);
        notasAlumnos.add(notaBilal);
        notasAlumnos.add(notaDani);
        notasAlumnos.add(notaHelena);
        return notasAlumnos;
    }
}
