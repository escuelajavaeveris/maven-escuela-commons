package escuela.java;

import org.junit.Assert;
import org.mockito.Spy;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

/**
 * Created by hgranjal on 12/06/2018.
 */
public class CalculoNotasTest {

    private static List<NotaAlumno> notasAlumnos;
    private CalculoNotas calculoNotas = new CalculoNotas();
    @Spy
    private CalculoNotas calculoNotasSpy = spy(calculoNotas);


    @BeforeMethod
    public void init() {
        notasAlumnos = AlumnosFactory.getAlumnos();
    }

    @Test
    public void testMediaInteros() {
        //no debria depender de las notas en la lista dummy
        assertEquals(calculoNotas.calcularMedia(notasAlumnos), 14f);
    }

    @Test
    public void testMediaDecimales() {
        //no debria depender de las notas en la lista dummy
        notasAlumnos.add(new NotaAlumno("Patrici", 15));
        assertEquals(calculoNotas.calcularMedia(notasAlumnos), 14.25f);
    }

    @Test
    public void testVariancia() {
        when(calculoNotasSpy.calcularMedia(notasAlumnos)).thenReturn(14f);
        assertEquals(calculoNotasSpy.calcularVariancia(notasAlumnos), 42f);
    }
}
