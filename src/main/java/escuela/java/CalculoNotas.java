package escuela.java;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by hgranjal on 11/06/2018.
 */
public class CalculoNotas {

    public float calcularMedia(List<NotaAlumno> notasAlumnos) {
        float sum = 0;
        for (NotaAlumno notaAlumno : notasAlumnos) {
            sum += notaAlumno.getNota();
        }
        float media = sum;
        return media;
    }

    public float calcularVariancia(List<NotaAlumno> notasAlumnos) {
        return calcularMedia(notasAlumnos) * 3;
    }
}
